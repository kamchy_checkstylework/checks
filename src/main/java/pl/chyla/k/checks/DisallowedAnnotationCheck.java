package pl.chyla.k.checks;

import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.FullIdent;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;
import com.puppycrawl.tools.checkstyle.utils.AnnotationUtil;

import java.util.*;

/**
 * Fails if parsed class is annotated with one of coma-separated (fully qualified) annotation class names
 * given in "disallowedAnnotations" check parameter
 * */
public class DisallowedAnnotationCheck extends AbstractCheck {
    private static final int[] TOKENS = { TokenTypes.CLASS_DEF,
            TokenTypes.IMPORT, TokenTypes.STATIC_IMPORT };
    private List<String> disallowedAnnosNamesList = List.of();
    private String disallowedAnnotationsString = "";
    private List<String> importIdentifiers = new ArrayList<>();


    @Override
    public int[] getDefaultTokens() {
        return TOKENS;
    }

    @Override
    public int[] getAcceptableTokens() {
        return TOKENS;
    }

    @Override
    public int[] getRequiredTokens() {
        return new int[0];
    }

    public void setDisallowedAnnotations(String annos) {
        this.disallowedAnnotationsString = annos;
        disallowedAnnosNamesList = Arrays.asList(annos.split(","));
    }

    public String getDisallowedAnnotations() {
        return disallowedAnnotationsString;
    }


    @Override
    public void visitToken(DetailAST ast) {
        int astType = ast.getType();

        if (astType == TokenTypes.IMPORT || astType == TokenTypes.STATIC_IMPORT) {
            var id = FullIdent.createFullIdentBelow(ast);
            importIdentifiers.add(id.getText());

        } else if (astType == TokenTypes.CLASS_DEF) {
            checkAnnotationText(ast);
        }
    }

    private void checkAnnotationText(DetailAST annotation) {
        for (String disallowedName : disallowedAnnosNamesList) {
            boolean contains = annotationContainsName(annotation, disallowedName);
            if (contains) {
                log(annotation.getLineNo(), "Annotation " + disallowedName + " is not allowed.");
            }
        }
    }

    private boolean annotationContainsName(DetailAST annotation, String disallowedName) {
        String lastPart = lastPart(disallowedName);
        boolean fulllNameExists = AnnotationUtil.containsAnnotation(annotation, disallowedName);
        boolean shortNameExists = AnnotationUtil.containsAnnotation(annotation, lastPart);
        boolean shortNameMatches = checkShortNameMatches(lastPart, disallowedName, importIdentifiers);
        return fulllNameExists || (shortNameExists && shortNameMatches);

    }

    private String lastPart(String disallowedName) {
        return disallowedName.substring(disallowedName.lastIndexOf(".") + 1);
    }

    private boolean checkShortNameMatches(String lastPart, String disallowedName, List<String> importIdentifiers) {
        for (var id :  importIdentifiers) {

            String joinedImportAndLastPart = id.endsWith(lastPart) ? id : (id + lastPart);
            if (disallowedName.equals(joinedImportAndLastPart)) {
                return true;
            }
        }
        return false;
    }
}
